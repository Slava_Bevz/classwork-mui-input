import React, { useContext } from "react";
import uniqid from 'uniqid';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';



import { ContextInput } from "../../helpers";
import FormItem from "./FormInput";
import { textTransform } from "@mui/system";

const Form = React.memo(({handlerChange, handleSubmit}) =>  {

    const {theme} = useContext(ContextInput);

    return(
        <>
            <Box component="form" sx={{ '& .MuiTextField-root': { m: 1, width: '550px' }, }} noValidate onSubmit={handleSubmit}>
                {
                    theme.map(element => {
                        return <FormItem key={uniqid()} data={element} handlerChange={handlerChange}/>
                    })
                }
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    m: 1, 
                    width: '550px',
                }}>
                    <Button  
                    variant="outlined" 
                    sx={
                        { 
                        textTransform: 'none',
                        flexDirection: 'row-reverse' ,
                        fontWeight: 'bold',
                        fontSize: '14px',
                        border: '2px solid #F7892F',
                        borderRadius: '3px',
                        padding: '10px 43px',
                        color: '#F7892F',
                        boxShadow: 3,
                        '&:hover': {
                            border: '2px solid red',
                            color: 'red',
                            boxShadow: 3,
                        }
                    }} 
                    type="submit">
                        Save
                    </Button>
                </Box>
                
            </Box>
        </>
    );
});

export default Form;