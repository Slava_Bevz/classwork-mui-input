import React, { useState } from "react";

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

const FormInput = ({data, handlerChange}) => {
    const { helperText, autocom, error, id} = data;

    const [value, setValue] = useState('');

    const hendlerInput = () => e => {
        setValue(e.target.value);
    }

    const countries = data?.list?.map(({ country }) => country) || [];

    return(
        <>
            {
                autocom?
                <Autocomplete
                    disablePortal
                    options={countries}
                    renderInput={(params) => <TextField {...params} {...data} sx={{
                        border: '1px solid transparent',
                        borderRadius: '3px',
                        background: '#F7F7FA',
                    }}/>}
                />:
                <TextField
                    {...data}
                    variant='outlined'
                    helperText={error? helperText : ''}
                    value={value || data.value}
                    onChange={hendlerInput()}
                    onBlur={handlerChange(id)}
                    sx={{
                        border: '1px solid transparent',
                        borderRadius: '3px',
                        background: '#F7F7FA',
                        marginX: '20px'
                    }}
                />
            }
        </>
    )
}

export default FormInput;